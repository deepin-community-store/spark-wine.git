#!/bin/bash

# 导入 transhell.bashimport 脚本并加载调试模式
source ${SHELL_DIR}/bashimport/transhell.bashimport
load_transhell_debug

#########>>>>>>> 函数段
# 获取当前系统的发行版名称
Get_Dist_Name()
{
    if grep -Eqii "Deepin" /etc/issue || grep -Eq "Deepin" /etc/*-release; then
        DISTRO='Deepin'
    elif grep -Eqi "UnionTech" /etc/issue || grep -Eq "UnionTech" /etc/*-release; then
        DISTRO='UniontechOS'
    elif grep -Eqi "GXDE" /etc/issue || grep -Eq "GXDE" /etc/*-release; then
        DISTRO='GXDE'
    elif grep -Eqi "UOS" /etc/issue || grep -Eq "UOS" /etc/*-release; then
        DISTRO='UniontechOS'
    else
        DISTRO='OtherOS'
    fi
}

# 检测常见的缩放环境变量
Check_Scale_Env_Vars()
{
    if [ -n "$DEEPIN_WINE_SCALE" ]; then
        echo "$DEEPIN_WINE_SCALE"
        return 0
    elif [ -n "$GDK_SCALE" ]; then
        echo "$GDK_SCALE"
        return 0
    elif [ -n "$QT_SCALE_FACTOR" ]; then
        echo "$QT_SCALE_FACTOR"
        return 0
    elif [ -n "$WINIT_X11_SCALE_FACTOR" ]; then
        echo "$WINIT_X11_SCALE_FACTOR"
        return 0
    elif [ -n "$ELM_SCALE" ]; then
        echo "$ELM_SCALE"
        return 0
    fi
    return 1
}
######<<<<<<<

# 提示全局参数的位置
echo "全局参数的位置在 $HOME/.config/spark-wine/scale.txt"

# 创建配置目录
mkdir -p $HOME/.config/spark-wine/

# 检查是否已经设置过全局参数
if [ -f "$HOME/.config/spark-wine/scale.txt" ]; then
    echo "已经设置过全局参数，使用"
    echo "全局参数的位置在 $HOME/.config/spark-wine/scale.txt，如果需要更换请删除此文件重新生成"
    exit
fi

# 检测常见的缩放环境变量
scale_value=$(Check_Scale_Env_Vars)
if [ -n "$scale_value" ]; then
    echo "$scale_value" > $HOME/.config/spark-wine/scale.txt
    echo "检测到缩放环境变量，使用 $scale_value 作为缩放比例"
    exit
fi

# 如果环境变量未设置，则根据系统发行版设置默认缩放比例
Get_Dist_Name
if [ "$DISTRO" = "UniontechOS" ] || [ "$DISTRO" = "GXDE" ]; then
    echo "1.0" > $HOME/.config/spark-wine/scale.txt
    echo "检测到 UniontechOS 或 GXDE 系统，使用 1.0 作为默认缩放比例"
    exit
fi

# 获取当前显示器的分辨率
dimensions=$(xdpyinfo | grep dimensions | sed -r 's/^[^0-9]*([0-9]+x[0-9]+).*$/\1/')

# 使用 Zenity 弹窗让用户选择缩放比例
scale_factor=$(zenity --list \
    --width=700 \
    --height=350 \
    --title="${TRANSHELL_CONTENT_YOUR_DIMENSION_IS} $dimensions ${TRANSHELL_CONTENT_PLEASE_CHOOSE_ONE_BELOW}" \
    --column="${TRANSHELL_CONTENT_OPTION}" \
    1.0 \
    1.25 \
    1.5 \
    1.75 \
    2.0  \
    2.5  \
    3.0  \
    3.5  \
    4.0)

# 处理用户的选择
case "$scale_factor" in 
    "")
        zenity --info --text="${TRANSHELL_CONTENT_1_SCALE_AS_DEFAULT}${TRANSHELL_CONTENT_YOU_CAN_USE_SPARK_WINE_HELPER_SETTINGS_TO_ADJUST}" --width=500 --height=150
        scale_factor="1.0"
        ;;
    *)
        zenity --info --text="${TRANSHELL_CONTENT_SCALE_IS} $scale_factor ${TRANSHELL_CONTENT_SAVED}！${TRANSHELL_CONTENT_YOU_CAN_USE_SPARK_WINE_HELPER_SETTINGS_TO_ADJUST}" --width=500 --height=150
        ;;
esac

# 将用户选择的缩放比例保存到配置文件中
echo "$scale_factor" > $HOME/.config/spark-wine/scale.txt